package org.example.actividad3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Actividad3 {

    public static void main(String[] args) {
        escribirFichero("Roberto Hidalgo Martínez");
    }

    private static void escribirFichero(String cadena) {
        File file = new File("resources/actividad3/fichero.txt");
        try {
            FileWriter fileWriter = new FileWriter(file,true);
            fileWriter.write(cadena);
            fileWriter.write("\n");
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
