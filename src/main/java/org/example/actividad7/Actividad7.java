package org.example.actividad7;

import java.io.*;
import java.util.ArrayList;

public class Actividad7 {

    public static void main(String[] args) {
        File file = new File("resources/actividad5/modulos.txt");
        ArrayList<String> modulos = leerFichero(file);
        escribirFichero(modulos);
    }

    private static ArrayList<String> leerFichero(File file) {
        try (FileReader fileReader = new FileReader(file);
             BufferedReader buffer = new BufferedReader(fileReader)){
            ArrayList<String> modulos = new ArrayList<>();
            do{
                String line = buffer.readLine();
                if (line == null) {
                    return modulos;
                }
                modulos.add(line);
            }while (true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    private static void escribirFichero(ArrayList<String> modulos) {
        File file = new File("resources/actividad5/modulosMayuscula.txt");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))){
            for (String modulo : modulos) {
                String moduloEnMayuscula = modulo.toUpperCase();
                bufferedWriter.write(moduloEnMayuscula);
                bufferedWriter.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
