package org.example.actividad1;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class FileManager {

    public static void crearFichero (String directorio, String archivo) throws IOException {
        File directory = new File(directorio);
        if (!directory.exists()) {
            throw new IOException("El directorio no existe");
        }
        File archive = new File(directorio, archivo);
        archive.createNewFile();
    }

    public static void verArchivosDirectorio(String directorio) {
        File directory = new File(directorio);
        if (!directory.exists()) {
            throw new RuntimeException("El directorio no existe");
        } else if (!directory.isDirectory()) {
            throw new RuntimeException("No es un directorio");
        }
        for (File file: directory.listFiles()) {
            System.out.println(file.getName());
        }
    }

    public static void verInformacionArchivo(String directorio, String archivo) {
        File file = new File(directorio, archivo);
        if (!file.exists()) {
            throw new RuntimeException("El fichero no existe");
        } else if (file.isDirectory()) {
            throw new RuntimeException("El fichero debe ser un archivo");
        }
        System.out.println("Nombre del archivo: " + file.getName());
        System.out.println("Ruta absoluta: " + file.getAbsolutePath());
        System.out.println("Ruta relativa: " + file.getPath());
        System.out.println("¿Tiene permiso de escritura?: " + file.canWrite());
        System.out.println("¿Tiene permiso de lectura?: " + file.canRead());
        System.out.println("¿Es un directorio?: " + file.isDirectory());
        System.out.println("¿Es un archivo?: " + file.isFile());
    }
}
