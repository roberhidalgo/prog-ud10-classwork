package org.example.actividad1;

import java.io.File;
import java.io.IOException;

public class Actividad1 {

    public static void main(String[] args) {

        try {
            FileManager.crearFichero("resources", "prueba1.txt");
            FileManager.crearFichero("resources", "prueba2.txt");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        FileManager.verArchivosDirectorio("resources");
        FileManager.verInformacionArchivo("resources", "prueba1.txt");
    }
}
