package org.example.actividad4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Actividad4 {

    public static void main(String[] args) {
        leerFichero();
    }

    private static void leerFichero() {
        try {
            int totalProductos = 0;
            File file = new File( "resources/actividad4/productos.txt");
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            do{
                String line = bufferedReader.readLine();
                if (line == null) {
                    System.out.println("El total de productos es " + totalProductos);
                    return;
                }
                String[] elementos = line.split(",");

                String producto = elementos[1].trim();
                int numProductos = Integer.parseInt(elementos[2].trim());

                if (producto.equals("Televisión") || producto.equals("Monitor") || producto.equals("Teclado"))
                    totalProductos += numProductos;
            }while (true);
        } catch (IOException e) {
            System.out.println("Se ha producido durante la lectura del archivo" + e.getMessage());
        }
    }
}
