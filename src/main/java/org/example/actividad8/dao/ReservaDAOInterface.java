package org.example.actividad8.dao;

import org.example.actividad8.exceptions.ReservaNotFoundException;
import org.example.actividad8.dto.Reserva;

import java.util.ArrayList;

public interface ReservaDAOInterface {

    ArrayList<Reserva> findAll();
    Reserva findById(String codigo);
    Reserva getById(String codigo) throws ReservaNotFoundException;
    ArrayList<Reserva> findByUser(String user);
    ArrayList<Reserva> findByTravel(String codViaje);
    boolean remove(Reserva reserva);
    boolean save(Reserva reserva);
}
