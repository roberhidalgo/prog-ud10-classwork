package org.example.actividad8.dao;

import org.example.actividad8.exceptions.ReservaNotFoundException;
import org.example.actividad8.dto.Reserva;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class FileReservaDAO implements ReservaDAOInterface{

    private File file;

    private static final String DATABASE_FILE = "resources/actividad8/reservas.txt";
    private static final String FIELD_SEPARATOR = ";";

    private static final int COL_CODIGO = 0;
    private static final int COL_NOMBRE = 1;
    private static final int COL_PLAZAS = 2;
    private static final int COL_FECHA = 3;

    public FileReservaDAO() {
        this.file = new File(DATABASE_FILE);
    }

    public FileReservaDAO(String pathToDBFile) {
        this.file = new File(pathToDBFile);
    }

    @Override
    public ArrayList<Reserva> findAll() {
        ArrayList<Reserva> reservas = new ArrayList<>();
        try {

            try (BufferedReader bufferedReader = getReader()) {
                do {
                    String reservaRegistro = bufferedReader.readLine();
                    if (reservaRegistro == null) {
                        return reservas;
                    }

                    Reserva reserva = getReservaFromRegister(reservaRegistro);
                    reservas.add(reserva);
                } while (true);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return reservas;
        }
    }

    @Override
    public Reserva findById(String codReserva) {
        try {
            try (BufferedReader bufferedReader = getReader()) {
                do {
                    String reservaRegistro = bufferedReader.readLine();
                    if (reservaRegistro == null) {
                        return null;
                    }

                    Reserva reserva = getReservaFromRegister(reservaRegistro);
                    if (reserva.getCodigoReserva().equals(codReserva)) {
                        return reserva;
                    }
                } while (true);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    private String getRegisterFromReserva(Reserva reserva) {
        return null;
    }

    private Reserva getReservaFromRegister(String reservaRegister) {
        String[] reservaFields = reservaRegister.split(FIELD_SEPARATOR);
        String codReserva = reservaFields[COL_CODIGO];
        String nombre = reservaFields[COL_NOMBRE];
        int plazasReservadas = Integer.parseInt(reservaFields[COL_PLAZAS]);
        LocalDateTime fechaRealizacion = LocalDateTime.parse(reservaFields[COL_FECHA],
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        return new Reserva(codReserva, nombre, plazasReservadas, fechaRealizacion);
    }

    @Override
    public Reserva getById(String codReserva) throws ReservaNotFoundException {
        try {
            try (BufferedReader bufferedReader = getReader()) {
                do {
                    String reservaRegistro = bufferedReader.readLine();
                    if (reservaRegistro == null) {
                        throw new ReservaNotFoundException(codReserva);
                    }

                    Reserva reserva = getReservaFromRegister(reservaRegistro);
                    if (reserva.getCodigoReserva().equals(codReserva)) {
                        return reserva;
                    }
                } while (true);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            throw new ReservaNotFoundException(codReserva);
        }
    }

    @Override
    public ArrayList<Reserva> findByUser(String user) {
        try {
            ArrayList<Reserva> reservas = new ArrayList<>();
            try (BufferedReader bufferedReader = getReader()) {
                do {
                    String reservaRegistro = bufferedReader.readLine();
                    if (reservaRegistro == null) {
                        return reservas;
                    }
                    Reserva reserva = getReservaFromRegister(reservaRegistro);
                    if (reserva.getUsuario().equals(user)) {
                        reservas.add(reserva);
                    }
                } while (true);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public ArrayList<Reserva> findByTravel(String codViaje) {
        try {
            ArrayList<Reserva> reservas = new ArrayList<>();
            try (BufferedReader bufferedReader = getReader()) {
                do {
                    String reservaRegistro = bufferedReader.readLine();
                    if (reservaRegistro == null) {
                        return reservas;
                    }
                    Reserva reserva = getReservaFromRegister(reservaRegistro);
                    if (reserva.perteneceAlViaje(codViaje)) {
                        reservas.add(reserva);
                    }
                } while (true);
            }
        } catch(IOException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public boolean remove(Reserva reserva) {
        try {
            boolean removed = false;
            ArrayList<Reserva> reservaslist = findAll();
            try (BufferedWriter bufferedWriter = getWriter(false)) {
                for (Reserva reservaItem : reservaslist) {
                    if (!reservaItem.equals(reserva)) {
                        bufferedWriter.write(getRegisterFromReserva(reservaItem));
                        bufferedWriter.newLine();
                    } else {
                        removed = true;
                    }
                }

                return removed;
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    private BufferedWriter getWriter(boolean append) throws IOException {
        return new BufferedWriter(new FileWriter(file, append));
    }

    @Override
    public boolean save(Reserva reserva) {
        return false;
    }

    private BufferedReader getReader() throws IOException {
        return new BufferedReader(new FileReader(file));
    }

}
