package org.example.actividad8;

import org.example.actividad8.dao.FileReservaDAO;
import org.example.actividad8.exceptions.ReservaNotFoundException;
import org.example.actividad8.dto.Reserva;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class TestReservas {

    private FileReservaDAO fileReservaDAO;

    private static final String[] FILE_INITIAL_REGISTERS = {
            "1-1;antonio;4;2022-04-03 23:56:06",
            "1-2;alex;3;2022-01-12 12:12:22",
            "2-1;roberto;5;2022-04-03 23:56:06",
            "2-2;roberto;5;2022-04-03 23:56:06"
    };

    private static final String PATH_TO_TEST_FILE = "resources/actividad8/test/reservas.txt";

    private static void prepareTestDBFile() {
        try {
            File file = new File(PATH_TO_TEST_FILE);
            try (BufferedWriter bufferedWriter = new BufferedWriter(
                    new FileWriter(file))) {
                for (String registerReserva: FILE_INITIAL_REGISTERS) {
                    bufferedWriter.write(registerReserva);
                    bufferedWriter.newLine();
                }
            }
        }catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void main(String[] args) {
        prepareTestDBFile();
        TestReservas testReservas = new TestReservas();
        testReservas.testFindById();
        testReservas.testGetById();
        testReservas.testFindAll();
        testReservas.testRemove();
    }

    public TestReservas() {

        this.fileReservaDAO = new FileReservaDAO(PATH_TO_TEST_FILE);
    }

    private void testFindById() {
        testFindById("1-2");
        testFindById("2-1");
    }

    private void testGetById() {
        testGetById("1-3");
        testGetById("3-0");
    }

    private void testFindById(String codTest) {
        Reserva reserva = this.fileReservaDAO.findById(codTest);

        if (reserva == null) {
            System.out.println("La reserva con código "
                    + codTest + "no ha sido encontrada");
        } else if (!reserva.getCodigoReserva().equals(codTest)) {
            System.out.println("Código esperado:" + codTest
                    + " Código obtenido: "+ reserva.getCodigoReserva());
        } else {
            System.out.println("OK");
        }
    }

    private void testGetById(String codTest) {
        try {
            Reserva reserva = this.fileReservaDAO.getById(codTest);
            if (reserva == null) {
                System.out.println("La reserva con código "
                        + codTest + "no ha sido encontrada");
            } else if (!reserva.getCodigoReserva().equals(codTest)) {
                System.out.println("Código esperado:" + codTest
                        + " Código obtenido: "+ reserva.getCodigoReserva());
            } else {
                System.out.println("OK");
            }
        }catch (ReservaNotFoundException ex) {
            System.out.println("Reserva " + codTest + " no encontrada");
        }
    }

    private void testFindAll() {
        ArrayList<Reserva> reservas = this.fileReservaDAO.findAll();

        int numReservasEsperadas = 3;
        if (reservas.size() != numReservasEsperadas) {
            System.out.println("Esperaba " + numReservasEsperadas + " reservas");
        } else if (!reservas.get(0).getCodigoReserva().equals("1-1")) {
            System.out.println("Esperaba primera reserva 1-1");
        } else {
            System.out.println("OK");
        }
    }

    private void testRemove() {

        // 1 - buscar una reserva existente (1-2) findById
        // comprobar que existe

        String codReserva = "1-2";
        Reserva reserva = this.fileReservaDAO.findById(codReserva);

        if (reserva == null) {
            System.out.println("La reserva " + codReserva + " no existe");
        } else {
            // 2 - borrar la reserva
            boolean eliminado = this.fileReservaDAO.remove(reserva);
            if (!eliminado) {
                System.out.println("La reserva " + codReserva
                        + " no se ha podido eliminar");
            } else {
                reserva = this.fileReservaDAO.findById(codReserva);
                if (reserva != null) {
                    System.out.println("La reserva " + codReserva
                            + " no se ha podido eliminar");
                }else {
                    System.out.println("La reserva " + codReserva
                            + " ha sido eliminada");
                }
            }

        }
    }

}
