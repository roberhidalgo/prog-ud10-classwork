package org.example.actividad6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.time.LocalDate;
import java.util.ArrayList;

public class Actividad6 {

    public static void main(String[] args) {
        File file = new File("resources/actividad6/alumnos.txt");
        ArrayList<String> infoAlumnos = leerFichero(file);
        ArrayList<Persona> alumnos = registrarAlumnos(infoAlumnos);
        mostrarAlumnos(alumnos);
    }

    private static void mostrarAlumnos(ArrayList<Persona> alumnos) {
        for (Persona alumno : alumnos) {
            System.out.println(alumno);
        }
    }

    private static ArrayList<Persona> registrarAlumnos(ArrayList<String> infoAlumnos) {
        ArrayList<Persona> alumnos = new ArrayList<>();
        for (String alumno : infoAlumnos) {
            String[] alumnoLinea = alumno.split(";");
            String nombre = alumnoLinea[0];
            String dni = alumnoLinea[1];
            String correo = alumnoLinea[2];
            LocalDate fecha = LocalDate.parse(alumnoLinea[3]);
            alumnos.add(new Persona(nombre, dni, correo, fecha));
        }
        return alumnos;
    }

    private static ArrayList<String> leerFichero(File file) {
        ArrayList<String> modulos = new ArrayList<>();
        try (FileReader fileReader = new FileReader(file);
              BufferedReader buffer = new BufferedReader(fileReader)){
            do{
                String line = buffer.readLine();
                if (line == null) {
                    return modulos;
                }
                modulos.add(line);
            } while (true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return modulos;
    }
}
