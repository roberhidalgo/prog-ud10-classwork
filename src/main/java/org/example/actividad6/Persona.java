package org.example.actividad6;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Persona {

    private String nombre;

    private String dni;

    private String correoElectronico;

    private LocalDate fechaNacimiento;

    public Persona(String nombre, String dni, String correoElectronico, LocalDate fechaNacimiento) {
        this.nombre = nombre;
        this.dni = dni;
        this.correoElectronico = correoElectronico;
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public String toString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return "Nombre: " + nombre +
                ", dni: " + dni +
                ", email: " + correoElectronico +
                ", fechaNacimiento: " + fechaNacimiento.format(formatter);
    }
}
